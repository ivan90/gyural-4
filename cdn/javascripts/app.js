/* Tutto Veicoli */
$(function() {

	$(document).trigger('dimensions');

	$(window).on('resize', function() {
		$(document).trigger('dimensions');
	});

});

$(document).on('dimensions', document, function() {
	map_placer_dimensions();
	console.log('a');
});

function map_placer_dimensions() {
	var selector = 'map';
	var width = $("#" + selector).closest('.column').width();
	$("#"+selector).width(width);
	$("#"+selector).height(width/1.1);
}

// Form Pre-Search \\
$(function() {
	$('form[data-presearch]').each(function() {

		$('input[data-presearch]', $(this)).each(function() {
			$(this).on('change', function(e,c,a) {
				$(this).closest('form[data-presearch]').trigger('pre-submit');
			});
		});

	});
});
/*
$(document).on('pre-submit', 'form[data-presearch]', function() {
	
	var params = $(this).serialize();
	var url = $(this).data('preurl');
	var form = this;
	$.post(url, params, function(res) {
		if(res.error == undefined) {

			var driver = $('[data-resdriver]', form);
			var element = $(driver).data('resdriver');
			var value = res[element];
			var output = driver.data('template').replace('#', value);
			
			if(value != 0) {
				if(driver.data('controller') == 'val') {
					driver.val(output);
				} else {
					driver.html(output);
				}
			}
			
		}
	}, 'json');

});
*/
/* MAP */
var wrld = {
    map: 'it_merc_en',
    zoomOnScroll: false,
    backgroundColor: 'transparent',
    regionStyle: {
    	initial: {
        	fill: '#0071BC'
      	},
		hover: {
			"fill": '#f7931e'
		},
	},
    onRegionClick: function(event, code) {
        $("[data-mapdrivenarea]").html(paths[code].name);
        $("input[name=regione]").val(code).trigger('change');
        //if(code == selectedCountry ) {
        // var data = {};
        // data[code] = "#f7931e";
        // $("#map").vectorMap("set", "fill", data);
    	//}
    }
};

$(function() {
	$('#map').vectorMap(wrld);
});