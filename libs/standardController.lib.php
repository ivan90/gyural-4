<?php

### GYURAL ###

/*

----------
Gyural 1.8
----------

Filename: /libs/standardController.lib.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 18/12/2013

----
Desc
----

The basic controller.

*/

class standardController {
	
	var $methods_prefix = array('App', 'Get', 'Ajax', 'Ctrl', 'Post', 'Put', 'Head', 'Api');
	var $index_tollerant = false;
	
	function __haveControllerAutotest($parts) {
		
		$realParts = explode('/', $parts);
		if($realParts[0] == $this->__className())
			unset($realParts[0]);
			
		$realParts2 = array_values($realParts);
		
		$realIndex = count($realParts);
		foreach($realParts2 as $index => $part) {
			$string = '';
			for($current = $index; $current < $realIndex; $current++) {
				$string .= '/' . $realParts2[$current-$index];
			}
			$portions[] = $string;
		}
		
		foreach($portions as $subCtrlCheck) {
			$parts = explode('/', $subCtrlCheck);
			$gP = null;
			foreach($parts as $part) {
				if(trim(strlen($part)) > 0)
					$gP[] = $part;
			}
			$latest = $gP[count($gP)-1];
			unset($gP[count($gP)-1]);
			$toTest[] = '/' . implode('_', $gP) . (strlen(implode('_', $gP)) > 0 ?'/':'') . $latest;
		}
		
		$portions = array_merge($toTest, $portions);
		$portions = array_unique($portions);
		
		foreach($portions as $v) {
			if($a = $this->__haveController($v))
				return $a;
		}
		
		return false;
		
	}
	
	function __className() {
		return str_replace('Ctrl', '', get_class($this));
	}
	
	function __haveController($parts) {
		
		if(!is_array($parts))
			$parts = explode('/', $parts);
		
		foreach($parts as $ipd => $parte) {
			if(strlen($parte) == 0)
				unset($parts[$ipd]);
		}
		reset($parts);
		$firstItem = key($parts);
		
		if($parts[$firstItem] == $this->__className())
			unset($parts[$firstItem]);
		
		$availableControllers = null;
		$controllerName = str_replace('-', '_', implode('__', $parts));
		
		foreach($this->methods_prefix as $method) {
			
			$methodTrying = $method . ucfirst($controllerName);
			
			if(method_exists($this, $methodTrying))
				$availableControllers[$method] = get_class($this) . '/' . $methodTrying;
			
		}
		
		return($availableControllers);
		
	}	
	
	function __exec($method, $type) {
		$this->$method();
		return uniqid();
	}
	
	
}

?>