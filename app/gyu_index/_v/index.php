<?php

$app_data["head"]["title"] = $app_data["head"]["title"] ? $app_data["head"]["title"] : siteName;
$app_data["head"]["description"] = $app_data["head"]["description"] ? $app_data["head"]["description"] : siteName;

$app_data["head"]["js"][] = 'jquery-1.10.2';

?>
<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<title><? echo $app_data["head"]["title"]; ?></title>
		<meta name="description" content="<? echo $app_data["head"]["description"]; ?>">
		<meta http-equiv="x-ua-compatible" content="ie=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
		<link href="//cdn.gyural.com/css/style.css" type="text/css" charset="utf-8" rel="stylesheet" media="screen" title="no title">
		<script src="<? echo CallFunction('gyu_optimization', 'js', $app_data["head"]["js"]); ?>" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<div class="row">
			<div class="left">
				<h2>Gyural <? echo version; ?></h2>
				<h1><? echo $app_data["head"]["title"]; ?></h1>
				<ul class="menu">
					<li><a href="//wiki.gyural.com" target="_blank">wiki.Gyural.com</a></li>
					<li><a href="//jquery.com" target="_blank">jQuery</a></li>
					<li><a href="//github.com/PHPMailer/PHPMailer">PHPMailer</a></li>
				</ul>
			</div>
			<div class="right">
				<div class="container">
					<h3 class="subs">Thank you to use Gyural.</h3>
					<p><? echo nl2br(file_get_contents(absolute . 'changelog.txt')); ?></p>

					<h3 class="subs"><a href="license.txt">License</a></h3>

					<h3 class="subs">Credits (for external classes)</h3>
					<ul>
						<li>lessphp (http://leafo.net/lessphp) — LESS css compiler, adapted from http://lesscss.org</li>
						<li>JSMin (http://code.google.com/p/jsmin-php/) — JavaScript Minifier</li>
					</ul>
				</div>
			</div>
		</div>
	</body>
</script>