<?php

function gyu_bucket__cert_exist() {
	return is_file(application . 'gyu_bucket/_certificates/cert.gcert');
}

function gyu_bucket__previsionedApp() {

	if ($handle = opendir(application . 'gyu_bucket/_app_prev/')) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry[0] != ".") {
				if(is_file(application . 'gyu_bucket/_app_prev/' . $entry)) {

					$open = zip_open(application . 'gyu_bucket/_app_prev/' . $entry);
					
					while($zip = zip_read($open)) {
						$file = zip_entry_name($zip);
						if($file == '_/version.gapp') {
							$content = CallFunction('gyu_bucket', 'appConf', 'a', 0, zip_entry_read($zip));
						}

					}

					$prev[] = array($entry, $content["name"], $content);
				}
			}
		}
		closedir($handle);
	}
	return $prev;

}

function gyu_bucket__isInstalled($pid) {

	$pidN = application . 'gyu_bucket/_installedApp/' . $pid;

	if(is_file($pidN)) {

		$content = file_get_contents($pidN);
		$o = 1;
		if($content[0] == '*') {
			list($a, $content) = explode('*', $content);
			$o = 0;
		}

		return array($content, $o);

	}
	return false;

}

function gyu_bucket__appConf($appName, $force = null, $parsed = null) {

	if($parsed == null) {

		if($force)
			$gyu_bucket_conf = $appName;
		else
			$gyu_bucket_conf = application . $appName . '/_/version.gapp';

	}
	
	if(is_file($gyu_bucket_conf) || $parsed != null) {
		
		if($parsed == null) {
			$lines = file($gyu_bucket_conf);
		} else {
			$lines = explode("\n", $parsed);
		}

		foreach($lines as $line) {
			list($key, $value) = explode(':', $line);
			$value = str_replace(array("\n", "\n\r"), '', $value);
			$key = trim($key);
			if(strlen(trim($value)) > 0) {
				if(strstr($value, '|')) {
					$value = explode('|', trim($value));
					$conf[$key] = $value;
				} else {
					$conf[$key] = trim($value);
				}
			}
		}

		return $conf;

	} else
		return false;

}

function gyu_bucket__validate($key, $cert = false) {
	
	if($cert == false) {
		
		$cert = application . 'gyu_bucket/_certificates/cert.gcert';
		
		if(!is_file($cert))
			header('Location: /gyu_bucket/makecert');
			
		$cert = md5_file(application . 'gyu_bucket/_certificates/cert.gcert');
		
	}
	
	if($key == $cert)
		$_SESSION["gyu_bucket"] = true;
	else
		$_SESSION["gyu_bucket"] = false;
	
}

function gyu_bucket__rlogged() {
	if(!CallFunction('gyu_bucket', 'logged'))
		die('have to be logged');
}

function gyu_bucket__logged() {
	return $_SESSION["gyu_bucket"];
}

function gyu_bucket__download($url, $name) {
	
	$r = md5(rand());
	
	$path = application . '/gyu_bucket/_tmp/' . $r . '.zip';
	
	$fp = fopen($path, 'w');
	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	$data = curl_exec($ch);
	
	curl_close($ch);
	file_put_contents($path, $data);
	
	return $r  . '@' . base64_encode($name);
	
}

function gyu_bucket__installed_app() {
	
	if ($handle = opendir(application)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				if(isApplication($entry) && is_dir(application . $entry)) {
					$object = null;
					$object["name"] = $entry;
					$object["detail"] = ApplicationDetail($entry);
					$installedApp[] = $object;
				}
			}
		}
		closedir($handle);
	}
	return $installedApp;
	
}

function gyu_bucket__installation() {
	
	if ($handle = opendir(absolute)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry[0] != "." || $entry == '.htaccess') {
				if(is_dir(absolute . $entry)) {
					$installedApp[] = '/' . $entry;
				} else $installedApp[] = $entry;
			}
		}
		closedir($handle);
	}
	return $installedApp;
	
}

function gyu_bucket__backup_presets() {
	
	if ($handle = opendir(application . 'gyu_bucket/backup/_presets')) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry[0] != ".") {
					$installedApp[] = $entry;
			}
		}
		closedir($handle);
	}
	return $installedApp;
	
}

function gyu_bucket__backups() {
	
	if ($handle = opendir(application . 'gyu_bucket/backup/_log/_zipped')) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry[0] != ".") {
				$installedApp[] = $entry;
			}
		}
		closedir($handle);
	}
	return $installedApp;
	
}

function gyu_bucket__pid($pid) {
	return 'http://localhost:8888/1.zip';
}

function gyu_bucket__Zip($source, $destination, $force = false)
{
	if (!extension_loaded('zip') || !file_exists($source)) {
		return false;
	}
	
	$zip = new ZipArchive();
	if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
		return false;
	}
	
	$source = str_replace('\\', '/', realpath($source));
	
	if (is_dir($source) === true)
	{
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
		
		foreach ($files as $file)
		{
			$file = str_replace('\\', '/', $file);
			if($force == false)
				if(strstr($file, 'gyu_bucket')) continue;
			// Ignore "." and ".." folders
			if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
			continue;
			
			$file = realpath($file);
			
			if (is_dir($file) === true)
			{
				$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
			}
			else if (is_file($file) === true)
			{
				$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
			}
			
		}
	}
	else if (is_file($source) === true)
	{
		$zip->addFromString(basename($source), file_get_contents($source));
	}
	
	return $zip->close();
	
}

function gyu_bucket__backupDB() {
	
	$dump = '-- Gyural ' . version . ' MySQL Dump --' . "\n" . '-- ' . date('r') . " -- \n\n";
	$dumpTables = '';
	$dumpElements = '';
	
	$tables = array();
	$results = FetchObject(Database()->query('SHOW TABLES'));

	foreach($results as $res) {
		$a = 0;
		foreach($res as $v) {
			if($a++ == 1)
				$tables[] = $v;
		}
	}

	foreach($tables as $table) {

		$q = Database()->query("SELECT * FROM `".$table."`");
		$allResults = FetchObject($q, 1);

		$dumpTables .= "\n-- creation of: `".$table."` --\n\n";
		$dumpTables .= FetchObject(Database()->query('SHOW CREATE TABLE ' . $table))->{'Create Table'} . ";\n\n";
		
		if($allResults) {
			$dumpElements .= "\n-- dump of `".$table."` --\n\n";
			foreach($allResults as $result) {
				$dumpElements .= CreateQuery('SI', $table, $result) . ";\n";
			}
		}
	}

	$dump .= $dumpTables . $dumpElements;

	return $dump;

}

## Optimize it ##
function getFilesFromDir($dir) { 

	$files = array();
	
	if ($handle = opendir($dir)) { 
		while (false !== ($file = readdir($handle))) { 
			if ($file != "." && $file != "..") { 
				if(is_dir($dir.'/'.$file)) { 
					$dir2 = $dir.'/'.$file; 
					$files[] = getFilesFromDir($dir2); 
				} else { 
				$files[] = $dir.'/'.$file; 
				} 
			}
		}
		closedir($handle); 
  	} 

	return array_flat($files); 

}

function array_flat($array) { 

	foreach($array as $a) { 
		if(is_array($a)) { 
			$tmp = array_merge($tmp, array_flat($a)); 
		} else { 
			$tmp[] = $a; 
		}
	}

	return $tmp; 
}