<?php

isGyu();
MethodStandard();

CallFunction('gyu_bucket', 'rlogged');

Application('gyu_bucket/_commons/header');

$presets = CallFunction('gyu_bucket', 'backup_presets');
$backups = CallFunction('gyu_bucket', 'backups');

$backupBase = application . 'gyu_bucket/backup/_log/_zipped/';

?>
<h4>Presets:</h4>
<? foreach($presets as $preset): ?>
<? echo $preset; ?> - [<a href="/gyu_bucket/backup/run/preset:<? echo $preset; ?>">Run</a>] [Edit]<br />
<? endforeach; ?>
<br />
<a href="/gyu_bucket/backup/wizard">Create New</a><br />
<a href="/gyu_bucket/backup/database">Backup Database, now</a>
<hr />
<h4>(Log) Backups:</h4>
<? foreach($backups as $backup): ?>
	<? echo $backup; ?> 
	<small>(filesize: <strong><? echo round(filesize($backupBase . $backup)/1024); ?>kB</strong> | 
		md5: <strong><? echo md5_file($backupBase . $backup); ?>)</strong></small>
	[<a href="<? echo str_replace(absolute, '/', $backupBase) . $backup; ?>">Download</a>] [Restore] [Upload] [move-to-GyuralCLOUD]
	<br />
<? endforeach; ?>
<hr />