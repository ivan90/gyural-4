<?php

isGyu();
MethodStandard();

Application('gyu_bucket/_commons/header');

if(!CallFunction('gyu_bucket', 'logged')):
	
	echo '<h1>Please, log-in.</h1>';
	if(!CallFunction('gyu_bucket', 'cert_exist'))
		echo '<a href="/gyu_bucket/makecert">Create your cert.</a>';
	die();
endif;

$appDet = ApplicationDetail('gyu_bucket');
?>
<small><strong>Gyural > 1</strong> Rename /_/funcs.php into /_/func.php (no-app-rep)</small><br />
<small><strong>Gyural <= 1</strong> Rename /_/funcs.php into /funcs/third/gyu_bucket.php</small>
<hr />

<strong>Version: <? echo($appDet["version"])?> <small><em>(<? echo($appDet["codename"])?>)</em></small></strong>
<h4>functionality</h4>
<pre>
<strong>Applications</strong>
[ ] Repository
[ ] Install an app from pid
[ ] Update an app
[x] List of installed apps
<strong>Backup</strong>
[x] Create Backups
[ ] Restore Backups
[ ] Schedule Backups
</pre>
<h3>how to use</h3>
<p>note that to use it you'll have to be logged.</p>
<dl>
	<!--<dt>Install an app</dt>
	<dd>/gyu_bucket/install/pid:<strong>pid</strong>/n:<strong>name</strong> (the pid of the app, is available on the repository)</dd>
	<dt>Update an app</dt>
	<dd>/gyu_bucket/update/pid:<strong>pid</strong>/n:<strong>name</strong> (not always available)</dd>-->
	<dt>List of installed apps</dt>
	<dd><a href="/gyu_bucket/list">/gyu_bucket/list</a></dd>
	<dt>App Repository</dt>
	<dd><a target="_blank" href="http://repository.gyural.speaki.at">repository.gyural.speaki.at</a></dd>
	<dt>Backup</dt>
	<dd><a href="/gyu_bucket/backup">/gyu_bucket/backup</a></dd>
	<dt>Login</dt>
	<dd>/gyu_bucket/login/k:<strong>key</strong></dd>
	<dt>Logout</dt>
	<dd><a href="/gyu_bucket/logout">/gyu_bucket/logout</a></dd>
</dl>