<?
if(version < implode(file('http://gyural.com/version'))) echo "<span style='color: green'>C'è una nuova versione di Gyural, http://gyural.com/repo</span>";
?>
<p class="lead">Bene, stiamo per installare la versione <? echo version; ?> di Gyural su questo spazio web.<br />
prima di iniziare, un paio di cose...</p>
<ul>
	<li><strong>Gyural</strong> funziona solo in ambienti Unix <em>(per via del modrewrite, e la gestione del filesystem.. libertà d'espansione)</em></li>
	<li><strong>Gyural</strong> necessita almeno di php 5.4 <em>(ok netsons, ok aruba, ok i cpanel in generale (distribuzioni centos))</em></li>
	<li><strong>Gyural</strong> necessita di un database di tipo mysql. <em>(tranne per la versione vetrina)</em></li>
</ul>
<h3 style="margin: 50px; font-size: 18px; text-align: center">Procediamo.</h3>
<center>
	<a class="button small" href="/setup/vetrina">versione vetrina</a>
	<a class="button" href="/setup/2">versione con database</a>
</center>