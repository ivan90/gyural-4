<?php

$app_data["head"]["title"] = $app_data["head"]["title"] ? $app_data["head"]["title"] : siteName;
$app_data["head"]["description"] = $app_data["head"]["description"] ? $app_data["head"]["description"] : siteName;

#$app_data["head"]["css"][] = '*/setup/_assets/style';
$app_data["head"]["js"][] = 'jquery-1.10.2';

?>
<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<title><? echo $app_data["head"]["title"]; ?></title>
		<meta name="description" content="<? echo $app_data["head"]["description"]; ?>">
		<meta http-equiv="x-ua-compatible" content="ie=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
		<link href="//cdn.gyural.com/css/style.css" type="text/css" charset="utf-8" rel="stylesheet" media="screen" title="no title">
		<script src="<? echo CallFunction('gyu_optimization', 'js', $app_data["head"]["js"]); ?>" type="text/javascript" charset="utf-8"></script>
	</head>
	<?php ob_flush(); ?>
	<body>

		<div class="row">
			<div class="left">
				<h2>Gyural <? echo version; ?></h2>
				<h1><? echo $app_data["head"]["title"]; ?></h1>
			</div>
			<div class="right">
				<div class="container">