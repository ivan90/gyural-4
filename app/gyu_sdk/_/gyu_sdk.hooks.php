<?php

CallFunction('hooks', 'set', 'gyu.created', 'gyu_sdk__created');
CallFunction('hooks', 'set', 'gyu.off', 'gyu_sdk__off');
CallFunction('hooks', 'set', 'gyu.off', 'deb_clean');
CallFunction('hooks', 'set', 'gyu.dev-zone', 'gyu_sdk__devPrint');

function gyu_sdk__created($time) {

	# Memory Usage #
	$GLOBALS["__memory"]["__startMemoryUsage"] = memory_get_usage();

	# Time #
	$GLOBALS["__exectime"]["__start"] = microtime(true);

}

function gyu_sdk__off($time) {

	# Memory #
	$GLOBALS["__memory"]["__finalMemoryUsage"] = memory_get_usage();
	$GLOBALS["__memory"]["__peakMemoryUsage"] = memory_get_peak_usage();

	$GLOBALS["__memory"]["delta"] = $GLOBALS["__memory"]["__peakMemoryUsage"] - $GLOBALS["__memory"]["__startMemoryUsage"];
	$GLOBALS["__memory"]["deltaInKB"] = $GLOBALS["__memory"]["delta"] / (1024*100);

	$GLOBALS["__memory"]["ten_user_resource_KB"] = $GLOBALS["__memory"]["deltaInKB"]*10;
	$GLOBALS["__memory"]["hundred_user_resource_KB"] = $GLOBALS["__memory"]["deltaInKB"]*100;
	$GLOBALS["__memory"]["thousand_user_resource_MB"] = ($GLOBALS["__memory"]["deltaInKB"]/1024)*1000;
	$GLOBALS["__memory"]["deltaInKB"] = $GLOBALS["__memory"]["delta"] / (1024*100);
	
	$GLOBALS["__memory"]["deltaInKB"] = round($GLOBALS["__memory"]["deltaInKB"]);

	# Time #
	$GLOBALS["__exectime"]["__end"] = microtime(true);
	$GLOBALS["__exectime"]["__duration"] = $GLOBALS["__exectime"]["__end"] - $GLOBALS["__exectime"]["__start"];

}

function gyu_sdk__devPrint($time, $env) {

	if(!$env->methods["get"] || MethodDetect('api') || MethodDetect('post'))
		return;

	echo '<pre><hr />';
	echo '<strong><u>Gyu Dev Center!</u></strong>' . "\n";

	echo '<em><u>Memory Usage</u></em>' . "\n";
	print_r($GLOBALS["__memory"]);

	echo "\n";

	echo '<em><u>Time Exec</u></em>' . "\n";
	print_r($GLOBALS["__exectime"]);

	echo "\n";
	echo '<em><u>Database</u></em>' . "\n";
	echo ($GLOBALS["num_query"] > 0 ? $GLOBALS["num_query"] : 0 . ' queries') . "\n";

	echo "\n";
	echo '<em><u>Environment</u></em>' . "\n";
	print_r($env);

	echo "\n\n";
	echo '<em><u>Method Detect</u></em>' . "\n";
	print_r(MethodDetect());

	echo "\n\n";
	echo '<em><u>Stack</u></em>' . "\n";
	print_r(deb_logs());

	echo '</pre>';

}

?>