<?php

/*

------------
Gyural 1.8.1
------------

Filename: /router.php
 Version: 1.8.1
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 5/06/14

----------

There's other method to set the route..

->map(url, destination, html_code [200 by default]); (this is the standard way...)
->b_map(url, destination, html_code [200 by default]); (this is the new-one!)
	
	ex:
		->b_map('/news/:id/:title/;lang', '/news/read/:id/:title/;lang', 200)

		:var - $_GET["var"]
		?;var - $_GET["var"] only if passed.

		Note: the "?;" vars are thinked to be at the end of the string.. and it's dangerous to use more than one per url...


Manipolate router with hooks

CallFunction('hooks', 'set', 'gyu.router.append', 'hook_func');

function hook_func($router) {
	$router->map(…, …);
}

*/

## top => down

$router = LoadClass('router', 1);

# Default from 1.4 #
$router->map('o/less/#.css', 'gyu_optimization/index/file:#/type:css');	// gyu_optimization
$router->map('o/js/#.js', 'gyu_optimization/index/file:#/type:js');		// gyu_optimization

$router->b_map('annuncio/:date/:categoryslug/:title/:id.html', 'annunci/get/:id');

$router->map('is_gyural', function() {
	echo 'yes it is.';
	echo 'v. '. version . '<br /><br />';
	echo '<a href="http://www.mandarinoadv.com">http://www.mandarinoadv.com</a><br />';
	echo '<a href="http://www.gyural.com">http://www.gyural.com</a>';
});

CallFunction('hooks', 'get', 'gyu.router.append', $router);

?>