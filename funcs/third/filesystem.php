<?php

/*

----------
Gyural 1.8
----------

Filename: /funcs/third/filesystem.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 11/10/13

----------------------
Collection: Filesystem
----------------------

*/

function filesystem__rrmdir($dir) { 
	if (is_dir($dir)) { 
		$objects = scandir($dir); 
		foreach ($objects as $object) { 
			if ($object != "." && $object != "..") { 
				if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
			}
		}
		reset($objects); 
		rmdir($dir); 
	}
}

function filesystem__upload() {

}

?>