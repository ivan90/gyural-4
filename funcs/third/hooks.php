<?php

/*

----------
Gyural 1.8
----------

Filename: /funcs/third/hooks.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 11/10/13

-----
Hooks
-----

# gyu_bucket and gyu_sdk required to use it. #

The idea is the same behind the wordpress's hooks.

You can specify an event in every point of the APP in wich other apps could inject is own code.
the mechanism is more like an event-listner..

The app trig an event: "common.header".

and from that point on all the listner for the "common.header" will be executed.

Realworld examples:

header.php:
<html>
	<head>
 		<? CallFunction('hooks', 'get', 'common.html.seo'); ?>
 		<link…>
 	</head>
</html>

/apps/seo/_/seo.hooks.php:
CallFunction('hooks', 'set', 'common.html.seo', 'functionName', priority);

/apps/otherapp/_/otherapp.hooks.php:
CallFunction('hooks', 'set', 'common.html.seo', 'otherFunctionName', priority);

http://www.rendered-example.com:
<html>
	<head>
		<title>My custom title</title>
	 	<description>I'm cool!</description>
	 </head>
</html>

@ @ @

__get($event)

__set($event, $function, $priority = 0)

__load() - if in dev mode, this function will ask everytime for a real-time list of hooks. (or disable it. 'hooks_cache')

__rebuild() - 

__empty() - flush the cache. http://www.example.com/gyu_sdk/hooks/flush (gyu_bucket and a valid certificate is required.)

*/

define('hooks_cache', true); // hooks_cache

function hooks__get($event) {

	$args = null;
	if(func_num_args() != 1) {
		$args = func_get_args();
		unset($args[0]);
	}
	
	CallFunction('hooks', 'dep');

	$triggers = CallFunction('hooks', 'load', $event);

	deb_log($event, 'hook');

	if(!is_array($triggers))
		return false;

	foreach($triggers as $trigger) {
		foreach($trigger as $item) {
			deb_log($item, 'hook-event:' . $event);

			if($args)
				call_user_func_array($item, $args);
			else
				call_user_func($item);
		}
	}

}

function hooks__set($event, $function, $priority = 0) {

	CallFunction('hooks', 'dep');
	$GLOBALS["gyu.sys_hooks"][$event][$priority][] = $function;
	
}

function hooks__load($attr = null) {

	CallFunction('hooks', 'dep');

	$listners = CallFunction('hooks', 'rebuilt');
	foreach($listners as $listner)
		include_once $listner;

	if($attr != null) {
		if(isset($GLOBALS["gyu.sys_hooks"][$attr]))
			return($GLOBALS["gyu.sys_hooks"][$attr]);
		else
			return false;
	} else
		return $GLOBALS["gyu.sys_hooks"];

}

function hooks__rebuilt() {

	CallFunction('hooks', 'dep');

	$cacheFile = cache . 'sys/hooks.cache';

	if(is_file($cacheFile) && hooks_cache)
		return unserialize(file_get_contents($cacheFile));

	$applications = CallFunction('gyu_bucket', 'installed_app');
	
	foreach($applications as $application) {
		$path = application.$application["name"].'/_/'.$application["name"].'.hooks.php';
		if(is_file($path)) {
			$return[] = $path;
		}
	}
	
	if(hooks_cache) {
		file_put_contents($cacheFile, serialize($return));
	} else {
		if(is_file($cacheFile))
			unlink($cacheFile);
	}

	return $return;

}

function hooks__empty() {

	CallFunction('hooks', 'dep');
	LoadClass('storage', 1)->clear('hooks');

}

function hooks__dep() {
	if(!isApplication('gyu_bucket') || !isApplication('gyu_sdk')) {
		echo 'hooks need gyu_bucket and gyu_sdk installed';
		return false;
	}
}

?>