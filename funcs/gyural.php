<?php

/*

----------
Gyural 1.8
----------

Filename: /funcs/gyural.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 21/11/13

----
Desc
----

The basic functions.


------------
gyu_autoload
------------

list all autoload .php into /funcs/autoload folder.

*/

function gyu_autoload() {
	$path = absolute . 'funcs/autoload/';
	if ($handle = opendir($path)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry[0] != "." && is_file($path . $entry)) { // + check on .php format.
				if(strstr($entry, '.php'))
					include_once $path . $entry;
			}
		}
		closedir($handle);
	}
}

/*

-----
Route
-----

Routing of the requests

*/

function Route(&$page, $app = false) {
	
	$GLOBALS["app"] = false;
	$goSafe = true;
	
	if($app)
		$GLOBALS["app"] = true;
	
	# Check if something is manually called.
	if($page != NULL)
		$_GET["x"] = $page;
	
	$environment = LoadClass('gyural', 1, MethodDetect());
	
	# The IndexApp is called when nothing it's requested.
	if(strlen($_GET["x"]) == 0)
		$_GET["x"] = IndexApp;
	if(is_file(absolute.'router.php')) {
		include absolute . 'router.php';
		$tmp = $router->_try($_GET["x"]);
	}
	$internalExecution = false;
	if(!is_string($tmp)) {
		$internalExecution = true;
	} else {
		$_GET["x"] = $tmp;
	}
	
	# Splitting $_GET["x"] by slash
	$parts = explode("/", $_GET["x"]);
	
	# Searching for /key:value/ remove it if found, then re-implode the "ipotetical app"
	foreach($parts as $part) {
		if(!strstr($part,varDivider)) {
			$file[] = $part;
			if(strlen($part) > 0)
				if($part[0] == '_')
					$goSafe = false;
		}
		else {
			@list($key, $value) = explode(varDivider, $part);
			$_GET[$key] = $value;
			$_REQUEST[$key] = $value;
		}
	}
	
	if(!$internalExecution) {
		if(!$file)
			$file = explode('/', IndexApp);
		$page = implode("/", $file);
	} else {
		$page = $tmp;
		$environment->internal_execution = true;
	}

	$environment->candidate = $page;

	# Check added into v1.4 #
	if($app == false && $goSafe == false)
		header('Location: /' . IndexApp);
	
	if($internalExecution)
		return $environment;
	// Else, go on with the standard checks.

	##############################################
	######## Guess the controllers trials ########
	$rounds = sizeof($file);
	for($index = 0; $index < $rounds; $index++) {
		$subCtrls[] = implode('_', $file);
		unset($file[sizeof($file)-1]);
	}
	##############################################
	
	foreach($subCtrls as $ctrls) {
		
		$environment->application = $ctrls;
		if($environment->controllerExists($environment->application)) {
			$environment->availableControllers = $environment->IsCallable($environment->Controller($environment->application)->__haveControllerAutotest($environment->candidate . '/index'), $environment->methods);
			if(!$environment->availableControllers) {
				if($environment->Controller($environment->application)->index_tollerant == false)
					$environment->availableControllersError = $environment->IsCallable($environment->Controller($environment->application)->__haveControllerAutotest(ErrorApp), $environment->methods);
				else {
					if(!$environment->availableControllers = $environment->IsCallable($environment->Controller($environment->application)->__haveControllerAutotest('index'), $environment->methods))
						$environment->availableControllersError = $environment->IsCallable($environment->Controller($environment->application)->__haveControllerAutotest(ErrorApp), $environment->methods);
				}
			} else
				break;
		}
	}

	if(!$environment->availableApplication = IsApplicationCallable($page)) {
		if(!$environment->availableApplicationError = IsApplicationCallable($environment->application . '/' . ErrorApp))
			$environment->availableApplicationError = IsApplicationCallable(ErrorApp);
	}
	
	return $environment;
	
}

/*

-----
isGyu
-----

this function is a tiny check the legitimate of a call.

*/

# Check if it's a gyural applications
function isGyu() {
	!defined('gyu') ? die("Only darkness after this point.") : '';
}

/*

-------
__error
-------

Exception Handler.

*/

function __error($e) {
	echo '<p>'.$e->getMessage().'</p>';
}


?>