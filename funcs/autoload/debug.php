<?php

$GLOBALS["__stack"] = array();
$GLOBALS["__sdkLogged"] = time().md5(rand());

function deb_log($message, $type) {

	$th = array(time(), $message, $type);
	$GLOBALS["__stack"][] = $th;
	$file = cache . 'sys/test/log-' . $GLOBALS["__sdkLogged"];
	if(dev && logStack)
		file_put_contents($file, json_encode($th) . "\n", FILE_APPEND);

}

function deb_logs() {

	return $GLOBALS["__stack"];

}

function deb_clean() {

	$file = cache . 'sys/test/log-' . $GLOBALS["__sdkLogged"];
	if(is_file($file)) {
		unlink($file);
	}

}

?>